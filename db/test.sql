CREATE DATABASE testapirest;

USE testapirest;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int (10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  
  `created_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;
/*Indice el motor que se va utilizar*/
/*Que muestre cuando se creo y el otro cuando se actualizo , el collate es 
  su codificacion de caracteres, current_timestamp que se crea con el tiempo actual cuando se el crea el usuario*/
/*
show tables;

describe users;
*/