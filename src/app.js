const express = require('express');
//INCIALIZAMOS
const app=express();

const morgan = require('morgan');
const bodyParser = require('body-parser');

/*SETTINGS ,REVISA SI EL SERVIDOR TIENE UN PUERTO DEFINIDO , CASO CONTRARIO QUE ESCUCHE EN EL PUERTO 3000*/
app.set('port',process.env.PORT || 3000);

/***************************************************MIDDELWARE*********************************/
//LA APLICACION VA UTILIZAR MORGAN PARA MOSTRARME LOS MENSAJES POR CONSOLA
//AL DECIR ESTE CODIGO NOS DICE QUE UTILIZE MORGAN EN SU CONFIGURACION DE DESARROLLO DE DEVELOPMENT
app.use(morgan('dev'));
//PARA ENTENDER LAS PETICIONES, OSEA NORMALMENTE LAS APLICACIONES CLIENTES ME ENVIAN LOS DATOS O
//ME ENVIAN PETICIONES A TRAVES DE JSON ENTONCES YO QUIERO ENTENDER ESOS METODOS QUE ME ESTAN ENVIANDO
app.use(bodyParser.json());
/*********************************************************************************************/
/*ROUTES*/
/*RECIBE EL OBJETO APP*/
require('./routes/userRoutes')(app);



/*ESCUCHE EL PUERTO 3000*/
app.listen(app.get('port'),()=>{
	/*CON ESTO TENEMOS EL SERVIDOR FUNCIONANDO*/
	console.log('Server on port 3000');
});