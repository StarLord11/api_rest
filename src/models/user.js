/*CONECTAR A LA BD Y TAMBIEN DEFINIER ALGUNOS METODOS Y SE PUEDE DIVIDR EN UN PATRON MVC*/
/*CONECTAMOS MYSQL*/
const mysql = require('mysql');
connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'testapirest'
});
/*Creamos un objeto userModel, para agregar metodos para obtener los datos , almacenarlos*/
let userModel={};
userModel.getUsers =(callback)=>{
if(connection){

	connection.query('SELECT * FROM users ORDER BY id',
	(err,rows)=>{
		if(err){
			throw err
		}
		else{
			callback(null,rows);/*caso contrario ejecute el error como null y ejecute las filas*/
		}
	}
	)
}
};
/*se necesita los datos del usuario */
userModel.insertUser=(userData,callback)=>{
	if(connection){
		connection.query(
'INSERT INTO users set ?',userData,
	(err,result)=>{
		if(err){
			throw err;
		}
		else{
			callback(null,{
				'insertId' : result.insertId
			})
		}
	}
	)
	}

};


userModel.updateUser=(userData,callback)=>{
	if(connection){
/*username es lo que obtenga , escape es para no proteger del sqlinjecetion*/
		
		const sql = `UPDATE users SET
		username=${connection.escape(userData.username)},
		password=${connection.escape(userData.password)},
		email=${connection.escape(userData.email)}
		where id=${connection.escape(userData.id)}
		`
/*Le pasamos la consulta sql , error y resultado*/
		connection.query(sql,(err,result)=>{
			if(err){
				throw err;
			}
			else{
				/*returna un collback con valores o un mensaje*/
				callback(null,{
				"msg":"Dato actualizado"
				});
				
			}
		});
	}
};
userModel.deleteUser=(id,callback)=>{
		if(connection){
			let sql = `SELECT * FROM users where id= ${connection.escape(id)}`;
			
			connection.query(sql,(err,row)=>{
			if(row){
				let sql= `DELETE FROM users where id=${id}`;
			connection.query(sql,(err,result)=>{
			if(err){
				throw err;
			}else{
				callback(null,{
					msg:'Ha sido eliminado'
				})
			}
			})
			}
		else{
		callback(null,{
		msg:'No existe'
			});
			}
});
}
}


/*Lo vamos a requerir en las rutas*/
module.exports=userModel;

