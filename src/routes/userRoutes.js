/*************PARA SER LAS RUTAS PARA NUESTROS USUARIOS****************/
//const express = require('express');
//Un modulo enrutador que lo que hace es definir rutas de mi servidor , entonces viende desde express
//y se ejecuta el metodo router ; a partir de aqui voy a exportar este router porque en realidad
//es un objeto ; y luego aqui dentro voy a decirle que quiero escriber las rutas de mi servidor .
//Ahora cuando mi enrutador o mi aplicacion en realidad , mi servidor reciba una peticion get a la ruta inicial
//de mi servidor que ejecute un funcion anonima , un manejador de peticiones que muestre por 
//respuesta un json que diga res.json y que de un arreglo vacio como respuesta
/*
const router = express.Router();

router.get('/',(req,res)=>{
 res.json([]);
}); 


module.exports= router;
*/
const User = require('../models/user');/*Para poder acceder los metodos getUsers*/
/*RECIBIMOS UN OBJETO APP QUE YA ESTA DEFINIDO EN EXPRESS*/
module.exports=function(app){

app.get('/users',(req,res)=>{
 User.getUsers((err,data)=>{/*PUEDO ACCEDER EL ERROR O LOS DATOS , SI RECIBIMOS ERROR SE PARA LA APLICACION*/
	res.status(200).json(data);			/*ENVIAMOS LOS DATOS , JSON POR DEFECTO MANDA AL ESTADO 200*/
 });
}); 


/*Cuando recibimos un post se va a ejecutar el req.body que me permite enviar los datos */
/*Los clientes envian datos json al servidor , en este caso el servidor solo esta revibiendo datos */
app.post('/users',(req,res)=>{
//console.log(req.body); recibimos datos nada mas 
const userData={
	id:null,
	username:req.body.username,
	password:req.body.password,
	email:req.body.email,
	created_at:null,/*eso lo crea la bd*/
	updated_at:null
};


/*Pasamos los datos*/
/*Obtenemos el userdata y El collback que lo va a obtener un mensaje json que le he dicho q es un id y q el user
no solo voy a obtener un objeto json y se puede devolver eso , la cual puedo obtner error o datos */

User.insertUser(userData,(err,data)=>{
if(data && data.insertId){
	console.log(data);
	/*Si tengo estos datos respondo algo correctamente y le enviamos un objeto al nevagador*/
res.json({
	success:true,
	msg:'Usuario Insertado',
	data:data
});
}else{
	/*En el caso q sale error , le ponemos un estado 500 ya que ha sido un error interno del servidor , y luego enviamos un json 
	de respuesta y luego un success false poque no ha sido satisfactorio*/
	res.status(500).json({
	success :false,
	msg: 'Error'
	});
}
});

});
/*Ponemos un metodo put son para cual se actualizan los datos y luego obtenemos un req y resp*/
//app.put('/users',(req,res)=>{
	app.put('/users/:id',(req,res)=>{
	/*Debemos recibir datos */
	const userData={
	//id:req.body.id,/*se va encargar de actualizar, tambien se puede mandar a la ruta*/
	id:req.params.id,
	username:req.body.username,
	password:req.body.password,
	email:req.body.email,
	created_at:null,/*eso lo crea la bd*/
	updated_at:null
};
/*Obtenemos los errores y los datos*/
	User.updateUser(userData,(err,data)=>{
	if(data && data.msg){
	res.json(data);
	}
	else{
		res.json({
			success: false,
			msg:'error'
		})
	}
	});
});

app.delete('/users/:id',(req,res)=>{
	User.deleteUser(req.params.id,(err,data)=>{
/*Comprobamos si existe los datos */
if(data && data.msg ==='Ha sido eliminado' ||  data.msg==='No existe'){
	/*si todo va bien envia*/
	res.json({
		success: true,
		data
	})
	}
	else{
		/*caso contrario enviamos un json con el estado 500 de error interno y el mensaje error*/
		res.status(500).json({
		msg:'error'
		})
		
	}
	});

});


};
